package com.james.cbs.seed.iservice;

import com.james.cbs.seed.dto.StateDto;
import com.james.cbs.seed.model.Lga;
import com.james.cbs.seed.model.State;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface IStateService {
    State findOne(String uuid);

    StateDto mapToDto(State model);

    State mapToModel(StateDto stateDto);

    List<Lga> getLgaFromState(State state);

    ResponseEntity<Void> createOneWithLgas(StateDto stateDto);
}