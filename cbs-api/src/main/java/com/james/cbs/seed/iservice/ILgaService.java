package com.james.cbs.seed.iservice;

import com.james.cbs.seed.dto.LgaDto;
import com.james.cbs.seed.model.Lga;
import com.james.cbs.seed.model.State;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface ILgaService {
    Lga findOne(String uuid);

    LgaDto mapToDto(Lga model);

    Lga mapToModel(LgaDto dto);

    ResponseEntity<Void> createOne(LgaDto lgaDto, String stateUUid);

    List<Lga> createMany(List<LgaDto> lgaDos, State state);

    ResponseEntity<List<LgaDto>> findLgaByState(String stateUUid);
}
