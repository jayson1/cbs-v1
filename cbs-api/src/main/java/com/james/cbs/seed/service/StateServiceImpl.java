package com.james.cbs.seed.service;

import com.james.cbs.seed.dto.LgaDto;
import com.james.cbs.seed.dto.StateDto;
import com.james.cbs.seed.iservice.IStateService;
import com.james.cbs.seed.model.Lga;
import com.james.cbs.seed.model.State;
import com.james.cbs.seed.repository.StateRepository;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service @Slf4j
public class StateServiceImpl implements IStateService {
    @Autowired
    private StateRepository stateRepository;
    @Autowired
    private LgaServiceImpl lgaService;

    @Override
    public State findOne(String uuid) {
        Optional<State> optional = this.stateRepository.findByUuid(uuid);
        if (optional.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        return optional.get();
    }

    @Override
    public StateDto mapToDto(State model) {
        StateDto dto = new StateDto();
        if (ObjectUtils.isNotEmpty(model.getUuid())) {
            dto.setUuid(model.getUuid());
        }
        if (ObjectUtils.isNotEmpty(model.getTitle())) {
            dto.setTitle(model.getTitle());
        }
        /*
        if (ObjectUtils.isNotEmpty(model.getLgaList()) && !model.getLgaList().isEmpty()) {
            dto.setLgaList(model.getLgaList().stream().map(lgaService::mapToDto).collect(Collectors.toList()));
        }
        */
        return dto;
    }

    @Override
    public State mapToModel(StateDto stateDto) {
        State state = new State();
        if (ObjectUtils.isNotEmpty(stateDto.getUuid())) {
            state.setUuid(stateDto.getUuid());
        }
        if (ObjectUtils.isNotEmpty(stateDto.getTitle())) {
            state.setTitle(stateDto.getTitle());
        }
        return state;
    }

    @Override
    public List<Lga> getLgaFromState(State state) {
        List<Lga> lgaList = new ArrayList<>();
        if (state.getLgaList().isEmpty()) {
            return new ArrayList<>();
        }
        for (Lga lga : state.getLgaList()) {
            lga.setState(null);
            lgaList.add(lga);
        }
        return lgaList;
    }

    @Override
    public ResponseEntity<Void> createOneWithLgas(StateDto stateDto) {
        try{
            State state = this.stateRepository.save(this.mapToModel(stateDto));
            this.lgaService.createMany(stateDto.getLgaList(), state);
            return ResponseEntity.status(HttpStatus.OK).build();
        }catch (Exception e){
            log.error(e.getMessage(), e);
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }
}
