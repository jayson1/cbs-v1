package com.james.cbs.seed.model;


import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;

import java.util.UUID;

import static com.james.cbs.common.constant.TableConstant.SEED_LGA_TABLE;

@Data
@Entity
@Table( name = SEED_LGA_TABLE)
@NoArgsConstructor
@ToString
public class Lga {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column
    private String uuid = UUID.randomUUID().toString();
    @Column
    private String title;
    @ManyToOne
    @JoinColumn(name = "state_id")
    private State state;
}
