package com.james.cbs.seed.controller;

import com.james.cbs.seed.dto.LgaDto;
import com.james.cbs.seed.dto.StateDto;
import com.james.cbs.seed.service.LgaServiceImpl;
import com.james.cbs.seed.service.StateServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static com.james.cbs.common.constant.RouteConstant.*;

@RestController
public class StateLgaController {
    @Autowired
    private StateServiceImpl stateService;
    @Autowired
    private LgaServiceImpl lgaService;

    @PostMapping(value = SEED_STATE_CREATE_ONE)
    public ResponseEntity<Void> createOneStateWithLgas(@RequestBody StateDto stateDto){
        return this.stateService.createOneWithLgas(stateDto);
    }

    @PostMapping(value = SEED_LGA_CREATE_ONE)
    public ResponseEntity<Void> createOneLga(@RequestBody LgaDto lgaDto, @PathVariable(value = "stateUuid") String stateUuid){
        return this.lgaService.createOne(lgaDto, stateUuid);
    }

    @GetMapping(value = SEED_STATE_GET_ONE)
    public ResponseEntity<StateDto> findOneState(@PathVariable String uuid){
        return null;
    }

    @GetMapping(value = SEED_LGA_GET_ONE)
    public ResponseEntity<LgaDto> findOneLga(@PathVariable String uuid){
        return null;
    }

    @GetMapping(value = SEED_LGA_GET_ALL_BY_STATE)
    public ResponseEntity<List<LgaDto>> findLgaByState(@PathVariable String stateUuid){
        return null;
    }

    @GetMapping(value = SEED_STATE_GET_ONE_BY_LGA)
    public ResponseEntity<StateDto> findStateByLga(String lgaUUID){
        return null;
    }
}
