package com.james.cbs.seed.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;

import static com.james.cbs.common.constant.TableConstant.SEED_STATE_TABLE;

@Data
@Entity
@Table( name = SEED_STATE_TABLE)
@NoArgsConstructor
@ToString
public class State {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column
    private String uuid = UUID.randomUUID().toString();
    @Column
    private String title;
    @OneToMany(mappedBy = "state")
    @Column
    private List<Lga> lgaList;
}
