package com.james.cbs.seed.service;

import com.james.cbs.seed.dto.LgaDto;
import com.james.cbs.seed.iservice.ILgaService;
import com.james.cbs.seed.model.Lga;
import com.james.cbs.seed.model.State;
import com.james.cbs.seed.repository.LgaRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.apache.commons.lang3.ObjectUtils.*;

@Service @Slf4j
public class LgaServiceImpl implements ILgaService {
    @Autowired
    private LgaRepository repository;
    @Autowired
    private StateServiceImpl stateService;

    @Override
    public Lga findOne(String uuid) {
        Optional<Lga> optional = this.repository.findByUuid(uuid);
        if (optional.isEmpty()){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        return optional.get();
    }

    @Override
    public LgaDto mapToDto(Lga model){
        LgaDto dto = new LgaDto();

        if (isNotEmpty(model.getUuid())){
            dto.setUuid(model.getUuid());
        }
        if (isNotEmpty(model.getTitle())){
            dto.setTitle(model.getTitle());
        }
        if (isNotEmpty(model.getState())){
            dto.setState(this.stateService.mapToDto(model.getState()));
        }
        return dto;
    }

    @Override
    public Lga mapToModel(LgaDto dto){
        Lga lga = new Lga();
        if (isNotEmpty(dto.getUuid())){
            lga.setUuid(dto.getUuid());
        }
        if (isNotEmpty(dto.getTitle())){
            lga.setTitle(dto.getTitle());
        }
        if (isNotEmpty(dto.getState())){
            lga.setState(this.stateService.findOne(dto.getUuid()));
        }
        return lga;
    }

    @Override
    public ResponseEntity<Void> createOne(LgaDto lgaDto, String stateUuid) {
        try {
            Lga lga = this.mapToModel(lgaDto);
            lga.setState(this.stateService.findOne(stateUuid));
            this.repository.save(lga);
            return ResponseEntity.status(HttpStatus.OK).build();
        }catch (Exception e){
            log.error(e.getMessage(), e);
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @Override
    public List<Lga> createMany(List<LgaDto> lgaDos, State state){
        List<Lga> lgaList = new ArrayList<>();
        if (!lgaDos.isEmpty()){
            for (LgaDto dto: lgaDos){
                Lga lga = this.mapToModel(dto);
                lga.setState(state);
                lgaList.add(lga);
                this.repository.save(lga);
            }
        }
        return lgaList;
    }

    @Override
    public ResponseEntity<List<LgaDto>> findLgaByState(String stateUUid){
        try {
            State state = this.stateService.findOne(stateUUid);
            List<LgaDto> list = new ArrayList<>();
            List<Lga> lgaList = this.repository.findAllByState(state);
            if (!lgaList.isEmpty()) {
                list = lgaList.stream().map(this::mapToDto).collect(Collectors.toList());
            }
            return ResponseEntity.ok(list);
        }catch (Exception e){
            log.error(e.getMessage(), e);
            return ResponseEntity.ok(new ArrayList<>());
        }
    }
}
