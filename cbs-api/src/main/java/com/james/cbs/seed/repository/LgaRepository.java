package com.james.cbs.seed.repository;

import com.james.cbs.seed.model.Lga;
import com.james.cbs.seed.model.State;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface LgaRepository extends JpaRepository<Lga, Long> {
    Optional<Lga> findByUuid(String uuid);
    List<Lga> findAllByState(State state);
}
