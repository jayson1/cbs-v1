package com.james.cbs.taxprofile.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.james.cbs.common.payload.Auditable;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;

import java.util.Date;

import static com.james.cbs.common.constant.TableConstant.TAX_PROFILE_TABLE;
import static javax.persistence.TemporalType.TIMESTAMP;

@Data
@Entity
@Table( name = TAX_PROFILE_TABLE)
@NoArgsConstructor
@ToString
public class TaxProfile {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "uuid", unique = true)
    private String uuid;

    @Column
    private String payerId;

    @Column
    private String name;

    @Column
    private String email;

    @Column
    private String phoneNumber;

    @Column
    private String tinNumber;

    @Column
    private String address;

    @Column
    private String taxPayerCode;

    @OneToOne
    @JoinColumn(name = "tax_profile_category_id")
    private TaxProfileCategory category;

    @JsonIgnore
    @CreatedDate
    @Temporal(TIMESTAMP)
    @Column(updatable = false)
    protected Date createdDate;

    @JsonIgnore
    @LastModifiedDate
    @Temporal(TIMESTAMP)
    protected Date lastModifiedDate;

}
