package com.james.cbs.taxprofile.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;

import java.util.Date;

import static com.james.cbs.common.constant.TableConstant.TAX_PROFILE_CATEGORY_TABLE;
import static javax.persistence.TemporalType.TIMESTAMP;

@Data
@Entity
@Table( name = TAX_PROFILE_CATEGORY_TABLE)
@NoArgsConstructor
@ToString
public class TaxProfileCategory {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "uuid", unique = true)
    private String uuid;

    @Column
    private String title;

    @OneToOne
    @JoinColumn(name = "parent_id")
    private TaxProfileCategory parent;

    @Column
    private boolean isChild;

    @JsonIgnore
    @CreatedDate
    @Temporal(TIMESTAMP)
    @Column(updatable = false)
    protected Date createdDate;

    @JsonIgnore
    @LastModifiedDate
    @Temporal(TIMESTAMP)
    protected Date lastModifiedDate;

}
