package com.james.cbs.bills.model;

import com.james.cbs.common.payload.Auditable;
import com.james.cbs.seed.model.Lga;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;

import java.util.UUID;

import static com.james.cbs.common.constant.TableConstant.BILLS_MDA_PROFILE_TABLE;

@Data
@Entity
@Table( name = BILLS_MDA_PROFILE_TABLE)
@NoArgsConstructor
@ToString
@EqualsAndHashCode( callSuper=true)
public class BillMdaProfile  extends Auditable<String> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "uuid", unique = true)
    private String uuid = UUID.randomUUID().toString();

    @Column(name = "code")
    private String code;

    @Column(name = "title")
    private String title;

    @Column(name = "email")
    private String email;

    @Column(name = "address")
    private String address;

    @Column(name = "pmb")
    private String pmb;

    @OneToOne
    @JoinColumn(name = "revenue_head_id")
    private BillMdaRevenueHead revenueHead;

    @OneToOne
    @JoinColumn(name = "lga_id")
    private Lga lga;
}
