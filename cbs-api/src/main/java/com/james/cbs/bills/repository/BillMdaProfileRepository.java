package com.james.cbs.bills.repository;

import com.james.cbs.bills.model.BillMdaProfile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface BillMdaProfileRepository extends JpaRepository<BillMdaProfile, Long> {
    Optional<BillMdaProfile> findByUuid(String uuid);
}
