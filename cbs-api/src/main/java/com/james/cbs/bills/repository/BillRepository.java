package com.james.cbs.bills.repository;

import com.james.cbs.bills.model.Bill;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface BillRepository extends JpaRepository<Bill, Long> {
    @Override
    Page<Bill> findAll(Pageable pageable);

    @Override
    List<Bill> findAll();

    Optional<Bill> findByUuid(String uuid);
}
