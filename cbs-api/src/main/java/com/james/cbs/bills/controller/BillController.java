package com.james.cbs.bills.controller;

import com.james.cbs.bills.dto.BillDto;
import com.james.cbs.bills.service.BillServiceImpl;
import com.james.cbs.common.payload.PageDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import java.util.List;

import static com.james.cbs.common.constant.RouteConstant.*;

@RestController
public class BillController {
    @Autowired
    private BillServiceImpl billService;

    @GetMapping(value = BILLS_GET_ALL)
    public ResponseEntity<List<BillDto>> getAll(){
        return this.billService.findAll();
    }

    @PostMapping(value = BILLS_CREATE_ONE)
    public ResponseEntity<Void> createOne(@RequestBody @Valid BillDto dto){
        return this.billService.createOne(dto);
    }

    @GetMapping(value = BILLS_GET_ONE)
    public ResponseEntity<BillDto> getOne(@PathVariable(value = "uuid") String uuid){
        return this.billService.findOne(uuid);
    }

    @PutMapping(value = BILLS_UPDATE_ONE)
    public void updateOne(){}

    @DeleteMapping(value = BILLS_DELETE_ONE)
    public void deleteOne(){}

    @PostMapping(value = BILLS_BATCH_UPLOAD)
    public void batchUpload(){}
}
