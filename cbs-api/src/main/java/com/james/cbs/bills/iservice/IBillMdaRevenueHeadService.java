package com.james.cbs.bills.iservice;

import com.james.cbs.bills.dto.BillMdaRevenueHeadDto;
import com.james.cbs.bills.model.BillMdaRevenueHead;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface IBillMdaRevenueHeadService {
    BillMdaRevenueHead findOneRaw(String uuid);

    ResponseEntity<BillMdaRevenueHeadDto> findOne(String uuid);

    ResponseEntity<Void> createOne(BillMdaRevenueHeadDto dto);

    ResponseEntity<Boolean> updateOne(BillMdaRevenueHeadDto dto);

    ResponseEntity<List<BillMdaRevenueHeadDto>> findAll();
}
