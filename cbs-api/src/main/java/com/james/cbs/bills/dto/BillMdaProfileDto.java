package com.james.cbs.bills.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.james.cbs.seed.dto.LgaDto;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
@JsonIgnoreProperties( ignoreUnknown = true)
@JsonInclude( JsonInclude.Include.NON_NULL)
public class BillMdaProfileDto {
    private Long id;
    private String uuid;
    private String code;
    @NotNull
    @NotEmpty
    private String title;
    private String email;
    private String address;
    private String pmb;
    @NotNull
    private BillMdaRevenueHeadDto revenueHead;
    private LgaDto lga;
}
