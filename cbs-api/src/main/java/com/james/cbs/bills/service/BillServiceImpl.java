package com.james.cbs.bills.service;

import com.james.cbs.bills.dto.BillDto;
import com.james.cbs.bills.iservice.IBillService;
import com.james.cbs.bills.model.Bill;
import com.james.cbs.bills.repository.BillRepository;
import com.james.cbs.common.service.CommonService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Slf4j
public class BillServiceImpl implements IBillService {
    @Autowired
    private BillRepository billRepository;
    @Autowired
    private BillMdaProfileServiceImpl profileService;
    @Autowired
    private CommonService commonService;


    @Override
    public ResponseEntity<List<BillDto>> findAll() {
        List<BillDto> dtoList = new ArrayList<>();
        List<Bill> bills = this.billRepository.findAll();
        if (!bills.isEmpty()){
            List<BillDto> list = new ArrayList<>();
            for (Bill bill : bills) {
                BillDto dto = mapToDto(bill);
                list.add(dto);
            }
            dtoList = list;
        }
        return ResponseEntity.status(HttpStatus.OK).body(dtoList);
    }

    @Override
    public ResponseEntity<Void> createOne(BillDto dto) {
        try {
            this.billRepository.save(this.mapToModel(dto));
            return ResponseEntity.status(HttpStatus.OK).build();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage());
        }
    }

    @Override
    public Bill findOneRawByUuid(String uuid){
        Optional<Bill> optional = this.billRepository.findByUuid(uuid);
        return optional.orElseThrow(()->new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

    @Override
    public ResponseEntity<BillDto> findOne(String uuid) {
        try {
            BillDto dto = this.mapToDto(this.findOneRawByUuid(uuid));
            return ResponseEntity.ok(dto);
        }catch (Exception e){
            log.error(e.getMessage(),e);
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @Override
    public ResponseEntity<Boolean> updateOne(BillDto dto) {
        return null;
    }

    @Override
    public ResponseEntity<Boolean> removeOne(String uuid) {
        return null;
    }

    @Override
    public ResponseEntity<Void> createMany(List<Bill> list) {
        return null;
    }

    @Override
    public ResponseEntity<String> uploadBatch(MultipartFile file) {
        return null;
    }

    @Override
    public BillDto mapToDto(Bill model) {
        BillDto dto = new BillDto();
        if (ObjectUtils.isNotEmpty(model.getUuid())) {
            dto.setUuid(model.getUuid());
        }
        if (ObjectUtils.isNotEmpty(model.getTittle())) {
            dto.setTittle(model.getTittle());
        }
        if (ObjectUtils.isNotEmpty(model.getCode())) {
            dto.setCode(model.getCode());
        }
        if (ObjectUtils.isNotEmpty(model.getAmount())) {
            dto.setAmount(model.getAmount());
        }
        if (ObjectUtils.isNotEmpty(model.getAmountInWords())) {
            dto.setAmountInWords(model.getAmountInWords());
        }
        if (ObjectUtils.isNotEmpty(model.getMaxAmount())) {
            dto.setMaxAmount(model.getMaxAmount());
        }
        if (ObjectUtils.isNotEmpty(model.getMinAmount())) {
            dto.setMinAmount(model.getMinAmount());
        }
        if (ObjectUtils.isNotEmpty(model.getIsAllowPartPayment())) {
            dto.setIsAllowPartPayment(model.getIsAllowPartPayment());
        }
        if (ObjectUtils.isNotEmpty(model.getMdaProfile())) {
            dto.setMdaProfile(this.profileService.mapToDto(model.getMdaProfile()));
        }

        return dto;
    }

    @Override
    public Bill mapToModel(BillDto billDto) {
        Bill bill = new Bill();
        if (ObjectUtils.isNotEmpty(billDto.getId())){
            bill.setId(billDto.getId());
        }
        if (ObjectUtils.isNotEmpty(billDto.getUuid())) {
            bill.setUuid(billDto.getUuid());
        }
        if (ObjectUtils.isNotEmpty(billDto.getTittle())) {
            bill.setTittle(billDto.getTittle());
        }
        if (ObjectUtils.isNotEmpty(billDto.getCode())) {
            bill.setCode(billDto.getCode());
        }
        if (ObjectUtils.isNotEmpty(billDto.getAmount())) {
            bill.setAmount(billDto.getAmount());
            bill.setAmountInWords(this.commonService.numberToWords(billDto.getAmount()));
        }
        if (ObjectUtils.isNotEmpty(billDto.getMaxAmount())) {
            bill.setMaxAmount(billDto.getMaxAmount());
        }
        if (ObjectUtils.isNotEmpty(billDto.getMinAmount())) {
            bill.setMinAmount(billDto.getMinAmount());
        }
        if (ObjectUtils.isNotEmpty(billDto.getIsAllowPartPayment())) {
            bill.setIsAllowPartPayment(billDto.getIsAllowPartPayment());
        }
        if (ObjectUtils.isNotEmpty(billDto.getMdaProfile())) {
            bill.setMdaProfile(this.profileService.findOne(billDto.getMdaProfile().getUuid()));
        }
        return bill;
    }

}
