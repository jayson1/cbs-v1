package com.james.cbs.bills.repository;

import com.james.cbs.bills.model.BillRemittaProfile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface BillRemittaProfileRepository extends JpaRepository<BillRemittaProfile, Long> {
    Optional<BillRemittaProfile> findByUuid(String uuid);
}
