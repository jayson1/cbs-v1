package com.james.cbs.bills.model;


import com.james.cbs.common.payload.Auditable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;

import java.util.UUID;

import static com.james.cbs.common.constant.TableConstant.BILLS_REMITTA_PROFILE_TABLE;

@Data
@Entity
@Table( name = BILLS_REMITTA_PROFILE_TABLE)
@NoArgsConstructor
@ToString
@EqualsAndHashCode( callSuper=true)
public class BillRemittaProfile  extends Auditable<String> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "code")
    private String code;

    @Column(name = "uuid", unique = true)
    private String uuid = UUID.randomUUID().toString();

    @Column(name = "token")
    private String token;

    @Column(name = "key")
    private String key;

    @Column(name = "secrete")
    private String secrete;

    @Column(name = "url")
    private String url;
}
