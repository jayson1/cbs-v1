package com.james.cbs.bills.controller;

import com.james.cbs.bills.dto.BillMdaProfileDto;
import com.james.cbs.bills.service.BillMdaProfileServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import static com.james.cbs.common.constant.RouteConstant.*;

@RestController
public class BillMdaProfileController {
    @Autowired
    private BillMdaProfileServiceImpl profileService;

    @PostMapping(value = BILLS_MDA_PROFILE_CREATE_ONE)
    public ResponseEntity<Void> createOne(@RequestBody @Valid BillMdaProfileDto dto) {
        return this.profileService.createOne(dto);
    }

    @PutMapping(value = BILLS_MDA_PROFILE_UPDATE_ONE)
    public ResponseEntity<Boolean> updateOne(@RequestBody BillMdaProfileDto dto) {
        return null;
    }

    @GetMapping(value = BILLS_MDA_PROFILE_GET_ALL)
    public ResponseEntity getAll() {
        return null;
    }

    @GetMapping(value = BILLS_MDA_PROFILE_GET_ONE)
    public ResponseEntity<BillMdaProfileDto> getOne(@PathVariable String uuid) {
        return null;
    }

    @PostMapping(value = BILLS_MDA_PROFILE_BATCH_UPLOAD)
    public ResponseEntity<Void> batchUpload() {
        return null;
    }
}
