package com.james.cbs.bills.iservice;

import com.james.cbs.bills.dto.BillMdaProfileDto;
import com.james.cbs.bills.model.BillMdaProfile;

public interface IBillMdaProfileService {
    BillMdaProfile findOne(String uuid);

    BillMdaProfileDto findOneToDto(String uuid);

    BillMdaProfileDto mapToDto(BillMdaProfile model);
}
