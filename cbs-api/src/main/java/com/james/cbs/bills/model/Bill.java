package com.james.cbs.bills.model;

import com.james.cbs.common.payload.Auditable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.apache.commons.lang3.ObjectUtils;

import javax.persistence.*;

import java.util.UUID;

import static com.james.cbs.common.constant.TableConstant.BILLS_TABLE;

@Data
@Entity
@Table( name = BILLS_TABLE)
@NoArgsConstructor
@ToString
@EqualsAndHashCode( callSuper=true)
public class Bill extends Auditable<String> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "uuid", unique = true)
    private String uuid;

    @Column(name = "title", nullable = false)
    private String tittle;

    @Column(name = "code")
    private String code;

    @Column(name = "amount")
    private Double amount;

    @Column(name = "amount_in_words")
    private String amountInWords;

    @Column(name = "max_amount")
    private Double maxAmount;

    @Column(name = "min_amount")
    private Double minAmount;

    @Column(name = "is_allow_part_payment")
    private Boolean isAllowPartPayment;

    @ManyToOne
    @JoinColumn(name = "mda_profile_id")
    private BillMdaProfile mdaProfile;

    @PrePersist
    public void onSetAmountInWords() {
        this.setUuid(UUID.randomUUID().toString());
    }
}
