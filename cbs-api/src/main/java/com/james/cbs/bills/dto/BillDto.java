package com.james.cbs.bills.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
@JsonIgnoreProperties( ignoreUnknown = true)
@JsonInclude( JsonInclude.Include.NON_NULL)
public class BillDto {
    private Long id;
    private String uuid;
    @NotNull
    private String tittle;
    private String code;
    private Double amount;
    private String amountInWords;
    private Double maxAmount;
    private Double minAmount;
    private Boolean isAllowPartPayment;
    private BillMdaProfileDto mdaProfile;
}
