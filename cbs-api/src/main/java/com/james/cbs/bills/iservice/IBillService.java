package com.james.cbs.bills.iservice;

import com.james.cbs.bills.dto.BillDto;
import com.james.cbs.bills.model.Bill;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface IBillService {
    ResponseEntity<List<BillDto>> findAll();

    ResponseEntity<Void> createOne(BillDto dto);

    Bill findOneRawByUuid(String uuid);

    ResponseEntity<BillDto> findOne(String uuid);

    ResponseEntity<Boolean> updateOne(BillDto dto);

    ResponseEntity<Boolean> removeOne(String uuid);

    ResponseEntity<Void> createMany(List<Bill> list);

    ResponseEntity<String> uploadBatch(MultipartFile file);

    BillDto mapToDto(Bill model);

    Bill mapToModel(BillDto billDto);
}
