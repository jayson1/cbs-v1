package com.james.cbs.bills.service;

import com.james.cbs.bills.dto.BillMdaRevenueHeadDto;
import com.james.cbs.bills.iservice.IBillMdaRevenueHeadService;
import com.james.cbs.bills.model.BillMdaRevenueHead;
import com.james.cbs.bills.repository.BillRevenueHeadRepository;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Slf4j
public class BillMdaRevenueHeadServiceImpl implements IBillMdaRevenueHeadService {
    @Autowired
    private BillRevenueHeadRepository repository;

    @Override
    public BillMdaRevenueHead findOneRaw(String uuid) {
        Optional<BillMdaRevenueHead> optional = this.repository.findByUuid(uuid);
        return optional.orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

    @Override
    public ResponseEntity<BillMdaRevenueHeadDto> findOne(String uuid) {
        try {
            BillMdaRevenueHead revenueHead = this.findOneRaw(uuid);
            BillMdaRevenueHeadDto dto = this.mapToDto(revenueHead);
            return ResponseEntity.status(HttpStatus.OK).body(dto);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @Override
    public ResponseEntity<Void> createOne(BillMdaRevenueHeadDto dto) {
        try {
            this.repository.save(this.mapToModel(dto));
            return ResponseEntity.status(HttpStatus.OK).build();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @Override
    public ResponseEntity<Boolean> updateOne(BillMdaRevenueHeadDto dto) {
        try {
            BillMdaRevenueHead oneRaw = this.findOneRaw(dto.getUuid());
            BillMdaRevenueHead revenueHead = this.mapToModel(dto);
            revenueHead.setId(oneRaw.getId());
            this.repository.save(revenueHead);
            return null;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @Override
    public ResponseEntity<List<BillMdaRevenueHeadDto>> findAll() {
        List<BillMdaRevenueHeadDto> list = new ArrayList<>();
        List<BillMdaRevenueHead> headLis = this.repository.findAll();
        if (!headLis.isEmpty()) {
            list = headLis.stream().map(this::mapToDto).collect(Collectors.toList());
        }
        return ResponseEntity.status(HttpStatus.OK).body(list);
    }

    public BillMdaRevenueHeadDto mapToDto(BillMdaRevenueHead model) {
        BillMdaRevenueHeadDto dto = new BillMdaRevenueHeadDto();
        if (ObjectUtils.isNotEmpty(model.getId())) {
            dto.setId(model.getId());
        }
        if (ObjectUtils.isNotEmpty(model.getUuid())) {
            dto.setUuid(model.getUuid());
        }
        if (ObjectUtils.isNotEmpty(model.getTitle())) {
            dto.setTitle(model.getTitle());
        }
        if (ObjectUtils.isNotEmpty(model.getCode())) {
            dto.setCode(model.getCode());
        }
        if (ObjectUtils.isNotEmpty(model.getContactAddress())) {
            dto.setContactAddress(model.getContactAddress());
        }
        if (ObjectUtils.isNotEmpty(model.getContactEmail())) {
            dto.setContactEmail(model.getContactEmail());
        }
        if (ObjectUtils.isNotEmpty(model.getContactName())) {
            dto.setContactName(model.getContactName());
        }
        if (ObjectUtils.isNotEmpty(model.getContactPhone())) {
            dto.setContactPhone(model.getContactPhone());
        }
        if (ObjectUtils.isNotEmpty(model.getOther())) {
            dto.setOther(model.getOther());
        }
        return dto;
    }

    public BillMdaRevenueHead mapToModel(BillMdaRevenueHeadDto headDto) {
        BillMdaRevenueHead head = new BillMdaRevenueHead();
        if (ObjectUtils.isNotEmpty(headDto.getId())) {
            head.setId(headDto.getId());
        }
        if (ObjectUtils.isNotEmpty(headDto.getUuid())) {
            head.setUuid(headDto.getUuid());
        }
        if (ObjectUtils.isNotEmpty(headDto.getTitle())) {
            head.setTitle(headDto.getTitle());
        }
        if (ObjectUtils.isNotEmpty(headDto.getCode())) {
            head.setCode(headDto.getCode());
        }
        if (ObjectUtils.isNotEmpty(headDto.getContactAddress())) {
            head.setContactAddress(headDto.getContactAddress());
        }
        if (ObjectUtils.isNotEmpty(headDto.getContactEmail())) {
            head.setContactEmail(headDto.getContactEmail());
        }
        if (ObjectUtils.isNotEmpty(headDto.getContactName())) {
            head.setContactName(headDto.getContactName());
        }
        if (ObjectUtils.isNotEmpty(headDto.getContactPhone())) {
            head.setContactPhone(headDto.getContactPhone());
        }
        if (ObjectUtils.isNotEmpty(headDto.getOther())) {
            head.setOther(headDto.getOther());
        }
        return head;
    }


}
