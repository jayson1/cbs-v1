package com.james.cbs.bills.repository;

import com.james.cbs.bills.model.BillMdaRevenueHead;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface BillRevenueHeadRepository extends JpaRepository<BillMdaRevenueHead, Long> {
    Optional<BillMdaRevenueHead> findByUuid(String uuid);
}
