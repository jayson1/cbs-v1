package com.james.cbs.bills.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
@JsonIgnoreProperties( ignoreUnknown = true)
@JsonInclude( JsonInclude.Include.NON_NULL)
public class BillRemittaProfileDto {
    private Long id;
    private String code;
    private String uuid;
    private String token;
    private String key;
    private String secrete;
    private String url;

}
