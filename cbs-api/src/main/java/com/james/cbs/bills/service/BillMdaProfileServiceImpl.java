package com.james.cbs.bills.service;

import com.james.cbs.bills.dto.BillMdaProfileDto;
import com.james.cbs.bills.iservice.IBillMdaProfileService;
import com.james.cbs.bills.model.BillMdaProfile;
import com.james.cbs.bills.repository.BillMdaProfileRepository;
import com.james.cbs.seed.service.LgaServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.Optional;

import static com.james.cbs.common.constant.ExceptionConstant.MDA_PROFILE_404;

@Service @Slf4j
public class BillMdaProfileServiceImpl  implements IBillMdaProfileService {
    @Autowired
    private BillMdaProfileRepository repository;
    @Autowired
    private BillMdaRevenueHeadServiceImpl revenueHeadService;
    @Autowired
    private LgaServiceImpl lgaService;

    public ResponseEntity<Void> createOne(BillMdaProfileDto dto){
        try{
            this.repository.save(this.mapToModel(dto));
            return ResponseEntity.status(HttpStatus.OK).build();
        }catch (Exception e){
            log.error(e.getMessage(), e);
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage());
        }
    }

    @Override
    public BillMdaProfile findOne(String uuid){
        Optional<BillMdaProfile> optional = this.repository.findByUuid(uuid);
        if (optional.isEmpty()){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, MDA_PROFILE_404);
        }
        return optional.get();
    }

    @Override
    public BillMdaProfileDto findOneToDto(String uuid){
        return this.mapToDto(this.findOne(uuid));
    }

    @Override
    public BillMdaProfileDto mapToDto(BillMdaProfile model){
        BillMdaProfileDto dto = new BillMdaProfileDto();
        if (ObjectUtils.isNotEmpty(model.getUuid())){
            dto.setUuid(model.getUuid());
        }
        if (ObjectUtils.isNotEmpty(model.getCode())){
            dto.setCode(model.getCode());
        }
        if (ObjectUtils.isNotEmpty(model.getTitle())){
            dto.setTitle(model.getTitle());
        }
        if (ObjectUtils.isNotEmpty(model.getEmail())){
            dto.setEmail(model.getEmail());
        }
        if (ObjectUtils.isNotEmpty(model.getAddress())){
            dto.setAddress(model.getAddress());
        }
        if (ObjectUtils.isNotEmpty(model.getPmb())){
            dto.setPmb(model.getPmb());
        }
        if (ObjectUtils.isNotEmpty(model.getRevenueHead())){
            dto.setRevenueHead(this.revenueHeadService.mapToDto(model.getRevenueHead()));
        }
        if (ObjectUtils.isNotEmpty(model.getLga())){
            dto.setLga(this.lgaService.mapToDto(model.getLga()));
        }
        return dto;
    }

    public BillMdaProfile mapToModel(BillMdaProfileDto dto){
        BillMdaProfile billMdaProfile = new BillMdaProfile();
        if (ObjectUtils.isNotEmpty(dto.getUuid())){
            billMdaProfile.setUuid(dto.getUuid());
        }
        if (ObjectUtils.isNotEmpty(dto.getCode())){
            billMdaProfile.setCode(dto.getCode());
        }
        if (ObjectUtils.isNotEmpty(dto.getTitle())){
            billMdaProfile.setTitle(dto.getTitle());
        }
        if (ObjectUtils.isNotEmpty(dto.getEmail())){
            billMdaProfile.setEmail(dto.getEmail());
        }
        if (ObjectUtils.isNotEmpty(dto.getAddress())){
            billMdaProfile.setAddress(dto.getAddress());
        }
        if (ObjectUtils.isNotEmpty(dto.getPmb())){
            billMdaProfile.setPmb(dto.getPmb());
        }
        if (ObjectUtils.isNotEmpty(dto.getRevenueHead())){
            billMdaProfile.setRevenueHead(this.revenueHeadService.findOneRaw(dto.getRevenueHead().getUuid()));
        }
        if (ObjectUtils.isNotEmpty(dto.getLga())){
            billMdaProfile.setLga(this.lgaService.findOne(dto.getLga().getUuid()));
        }
        return billMdaProfile;
    }

}
