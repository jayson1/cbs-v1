package com.james.cbs.bills.model;

import com.james.cbs.common.payload.Auditable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;

import java.util.UUID;

import static com.james.cbs.common.constant.TableConstant.BILLS_MDA_REVENUE_HEAD_TABLE;

@Data
@Entity
@Table( name = BILLS_MDA_REVENUE_HEAD_TABLE)
@NoArgsConstructor
@ToString
@EqualsAndHashCode( callSuper=true)
public class BillMdaRevenueHead extends Auditable<String> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "uuid", unique = true)
    private String uuid = UUID.randomUUID().toString();

    @Column(name = "title")
    private String title;

    @Column(name = "code")
    private String code;

    @Column(name = "contact_name")
    private String contactName;

    @Column(name = "contact_phone")
    private String contactPhone;

    @Column(name = "contact_email")
    private String contactEmail;

    @Column(name = "contact_address")
    private String contactAddress;

    @Column(name = "other")
    private String other;

    @OneToOne(mappedBy = "revenueHead", fetch = FetchType.EAGER)
    private BillMdaProfile profile;
}
