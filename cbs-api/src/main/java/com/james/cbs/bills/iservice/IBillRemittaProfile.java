package com.james.cbs.bills.iservice;

import com.james.cbs.bills.dto.BillRemittaProfileDto;
import com.james.cbs.bills.model.BillRemittaProfile;

public interface IBillRemittaProfile {
    BillRemittaProfile findOneUuid(String uuid);

    BillRemittaProfile createOne(BillRemittaProfileDto dto);

    BillRemittaProfileDto mapToDto(BillRemittaProfile model);

    BillRemittaProfile mapToModel(BillRemittaProfileDto profileDto);
}
