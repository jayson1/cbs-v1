package com.james.cbs.bills.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
@JsonIgnoreProperties( ignoreUnknown = true)
@JsonInclude( JsonInclude.Include.NON_NULL)
public class BillMdaRevenueHeadDto {
    private Long id;
    private String uuid;
    @NotNull
    @NotEmpty
    private String title;
    private String code;
    private String contactName;
    private String contactPhone;
    private String contactEmail;
    private String contactAddress;
    private String other;
}
