package com.james.cbs.bills.controller;

import com.james.cbs.bills.dto.BillMdaRevenueHeadDto;
import com.james.cbs.bills.service.BillMdaRevenueHeadServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import java.util.List;

import static com.james.cbs.common.constant.RouteConstant.*;

@RestController
public class BillMdaRevenueHeadController {
    @Autowired
    private BillMdaRevenueHeadServiceImpl service;

    @PostMapping(value = BILLS_REVENUE_HEAD_CREATE )
    public ResponseEntity<Void> createOne(@RequestBody @Valid BillMdaRevenueHeadDto dto){
        return this.service.createOne(dto);
    }

    @GetMapping(value = BILLS_REVENUE_HEAD_GET_ONE)
    public ResponseEntity<BillMdaRevenueHeadDto> getOne(@PathVariable(value = "uuid") String uuid){
        return this.service.findOne(uuid);
    }

    @GetMapping(value = BILLS_REVENUE_HEAD_GET_ALL)
    public ResponseEntity<List<BillMdaRevenueHeadDto>> getAll(){
        return this.service.findAll();
    }

    @PutMapping(value = BILLS_REVENUE_HEAD_UPDATE)
    public ResponseEntity<Boolean> updateOne(@RequestBody @Valid BillMdaRevenueHeadDto dto){
        return this.service.updateOne(dto);
    }

}
