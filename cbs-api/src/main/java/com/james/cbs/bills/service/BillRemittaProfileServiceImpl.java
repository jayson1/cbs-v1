package com.james.cbs.bills.service;

import com.james.cbs.bills.dto.BillRemittaProfileDto;
import com.james.cbs.bills.iservice.IBillRemittaProfile;
import com.james.cbs.bills.model.BillRemittaProfile;
import com.james.cbs.bills.repository.BillRemittaProfileRepository;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.Optional;

@Service
public class BillRemittaProfileServiceImpl  implements IBillRemittaProfile {
    @Autowired
    private BillRemittaProfileRepository repository;

    @Override
    public BillRemittaProfile findOneUuid(String uuid){
        Optional<BillRemittaProfile> optional = this.repository.findByUuid(uuid);
        return optional.orElseThrow(()->new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

    @Override
    public BillRemittaProfile createOne(BillRemittaProfileDto dto){
        try {
            return this.repository.save(this.mapToModel(dto));
        }catch (Exception e){
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public BillRemittaProfileDto mapToDto(BillRemittaProfile model){
        BillRemittaProfileDto dto = new BillRemittaProfileDto();
        if (ObjectUtils.isNotEmpty(model.getId())){
            dto.setId(model.getId());
        }
        if (ObjectUtils.isNotEmpty(model.getCode())){
            dto.setCode(model.getCode());
        }
        if (ObjectUtils.isNotEmpty(model.getUuid())){
            dto.setUuid(model.getUuid());
        }
        if (ObjectUtils.isNotEmpty(model.getToken())){
            dto.setToken(model.getToken());
        }
        if (ObjectUtils.isNotEmpty(model.getKey())){
            dto.setKey(model.getKey());
        }
        if (ObjectUtils.isNotEmpty(model.getSecrete())){
            dto.setSecrete(model.getSecrete());
        }
        if (ObjectUtils.isNotEmpty(model.getUrl())){
            dto.setUrl(model.getUrl());
        }
        return dto;
    }

    @Override
    public BillRemittaProfile mapToModel(BillRemittaProfileDto profileDto){
        BillRemittaProfile profile = new BillRemittaProfile();
        if (ObjectUtils.isNotEmpty(profileDto.getId())){
            profile.setId(profileDto.getId());
        }
        if (ObjectUtils.isNotEmpty(profileDto.getCode())){
            profile.setCode(profileDto.getCode());
        }
        if (ObjectUtils.isNotEmpty(profileDto.getUuid())){
            profile.setUuid(profileDto.getUuid());
        }
        if (ObjectUtils.isNotEmpty(profileDto.getToken())){
            profile.setToken(profileDto.getToken());
        }
        if (ObjectUtils.isNotEmpty(profileDto.getKey())){
            profile.setKey(profileDto.getKey());
        }
        if (ObjectUtils.isNotEmpty(profileDto.getSecrete())){
            profile.setSecrete(profileDto.getSecrete());
        }
        if (ObjectUtils.isNotEmpty(profileDto.getUrl())){
            profile.setUrl(profileDto.getUrl());
        }
        return profile;
    }
}
