package com.james.cbs;

import org.springframework.boot.autoconfigure.SpringBootApplication;


import static org.springframework.boot.SpringApplication.*;

@SpringBootApplication
public class CbsApplication {

	public static void main(String[] args) {
		run(CbsApplication.class, args);
	}

}
