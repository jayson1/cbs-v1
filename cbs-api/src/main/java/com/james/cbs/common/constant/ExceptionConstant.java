package com.james.cbs.common.constant;

public class ExceptionConstant {
    public static final String USER_404 = "Oops! User not found.";
    public static final String MDA_PROFILE_404 = "Oops! Mda Profile Not Found";
}
