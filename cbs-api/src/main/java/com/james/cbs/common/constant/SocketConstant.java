package com.james.cbs.common.constant;

public class SocketConstant {
    public static final String SOCKET_CLIENT = "http://localhost:4200";
    public static final String SOCKET_ENDPOINT = "/socket";
    public static final String SOCKET_DESTINATION_PREFIX = "app";
    public static final String SOCKET_TOPIC = "/topic";
    //test user destination
    public static final String DESTINATION_USER = SOCKET_TOPIC +"/user";

}
