package com.james.cbs.common.payload;

public class PageDto {
    private int totalItems;
    private int totalPages;
    private int currentPage;
}
