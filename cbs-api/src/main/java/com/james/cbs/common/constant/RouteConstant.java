package com.james.cbs.common.constant;

public class RouteConstant {
    public static final String API_PREFIX = "/api/v1";

    // bills route
    public static final String BILLS_PREFIX = API_PREFIX + "/bills";
    public static final String BILLS_GET_ALL = BILLS_PREFIX+"/all";
    public static final String BILLS_GET_ONE = BILLS_PREFIX+"/{uuid}";
    public static final String BILLS_CREATE_ONE = BILLS_PREFIX+"/create";
    public static final String BILLS_UPDATE_ONE = BILLS_PREFIX+"/update";
    public static final String BILLS_DELETE_ONE = BILLS_PREFIX+"/remove";
    public static final String BILLS_BATCH_UPLOAD = BILLS_PREFIX+"/batch-upload";

    // bills mda profile
    public static final String BILLS_MDA_PROFILE_PREFIX = BILLS_PREFIX + "/mda-profile";
    public static final String BILLS_MDA_PROFILE_CREATE_ONE = BILLS_MDA_PROFILE_PREFIX+"/create";
    public static final String BILLS_MDA_PROFILE_UPDATE_ONE = BILLS_MDA_PROFILE_PREFIX+"/update";
    public static final String BILLS_MDA_PROFILE_GET_ALL = BILLS_MDA_PROFILE_PREFIX+"/all";
    public static final String BILLS_MDA_PROFILE_GET_ONE = BILLS_MDA_PROFILE_PREFIX+"{uuid}";
    public static final String BILLS_MDA_PROFILE_BATCH_UPLOAD = BILLS_MDA_PROFILE_PREFIX+"/batch-upload";

    // bill revenue head
    public static final String BILLS_REVENUE_HEAD_PREFIX  =  BILLS_PREFIX +"/mda-revenue-head";
    public static final String BILLS_REVENUE_HEAD_CREATE  =  BILLS_REVENUE_HEAD_PREFIX +"/create";
    public static final String BILLS_REVENUE_HEAD_UPDATE  =  BILLS_REVENUE_HEAD_PREFIX +"/update";
    public static final String BILLS_REVENUE_HEAD_DELETE  =  BILLS_REVENUE_HEAD_PREFIX +"/remove/{uuid}";
    public static final String BILLS_REVENUE_HEAD_BATCH_UPLOAD  =  BILLS_REVENUE_HEAD_PREFIX +"/batch-upload";
    public static final String BILLS_REVENUE_HEAD_GET_ONE  =  BILLS_REVENUE_HEAD_PREFIX +"/{uuid}";
    public static final String BILLS_REVENUE_HEAD_GET_ALL  =  BILLS_REVENUE_HEAD_PREFIX +"/all";

    // seeds
    public static final String SEED_PREFIX = API_PREFIX + "/seeds";
    public static final String SEED_STATE_PREFIX = SEED_PREFIX + "/state";
    public static final String SEED_STATE_BATCH_UPLOAD = SEED_STATE_PREFIX + "/batch-upload";
    public static final String SEED_STATE_GET_ALL = SEED_STATE_PREFIX + "/all";
    public static final String SEED_STATE_GET_ONE = SEED_STATE_PREFIX + "/{uuid}";
    public static final String SEED_STATE_UPDATE_ONE = SEED_STATE_PREFIX + "/update";
    public static final String SEED_STATE_CREATE_ONE = SEED_STATE_PREFIX + "/create";
    public static final String SEED_STATE_GET_ONE_BY_LGA = SEED_STATE_PREFIX + "find-by-lga/{lgaUuid}";

    public static final String SEED_LGA_PREFIX = SEED_PREFIX + "/lga";
    public static final String SEED_LGA_GET_ALL_BY_STATE = SEED_LGA_PREFIX + "/list-lga-by-state/{stateUuid}";
    public static final String SEED_LGA_GET_ONE = SEED_LGA_PREFIX + "/{uuid}";
    public static final String SEED_LGA_BATCH_UPLOAD = SEED_LGA_PREFIX + "/batch-upload";
    public static final String SEED_LGA_CREATE_ONE = SEED_LGA_PREFIX + "/create/{stateUuid}";


}
