package com.james.cbs.common.constant;

public class SecurityConstant {
    public static final String APP_PREFIX = "CBS";
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String TOKEN_HEADER = "Authorization";
    public static final String TOKEN_CANNOT_BE_VERIFIED = "Token cannot be verified";
    public static final String GET_ARRAYS_ISSUER = APP_PREFIX+"_API";
    public static final String GET_USER_GROUP = APP_PREFIX+"_Users";
    public static final String AUTHORITIES = "Authorities";
    public static final String OPTIONS_HTTP_METHOD = "OPTIONS";
    public static final String WEB_ENDPOINT = "http://localhost:4200";
    public static final String FORBIDDEN = "You need to log in to gain access";
    public static final String ROLES = "Roles";
    public static final String ACCESS_DENIED_MESSAGE = "You don not have permission to access this page";

    public static final int JWT_DEFAULT_EXPIRE_IN = 1; //hours
}
