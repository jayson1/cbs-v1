package com.james.cbs.common.constant;

public class TableConstant {
    public static final String TABLE_PREFIX = "cbs";
    public static final String TABLE_SUFFIX = "record";
    // Authentication Module
    public static final String AUTH_TABLE_PREFIX = TABLE_PREFIX +"_auth";
    public static final String USER_TABLE = AUTH_TABLE_PREFIX + "_users_" + TABLE_SUFFIX;
    public static final String USER_ROLE_TABLE = AUTH_TABLE_PREFIX + "_users_roles_" + TABLE_SUFFIX;
    public static final String ROLE_TABLE = AUTH_TABLE_PREFIX + "_roles_" + TABLE_SUFFIX;
    public static final String PERMISSION_TABLE = AUTH_TABLE_PREFIX + "_permissions_" + TABLE_SUFFIX;
    public static final String ROLE_PERMISSION_TABLE = AUTH_TABLE_PREFIX + "_roles_permissions_" + TABLE_SUFFIX;

    // bills
    public static final String BILLS_TABLE_PREFIX = TABLE_PREFIX + "_bills";
    public static final String BILLS_TABLE = BILLS_TABLE_PREFIX +"_"+TABLE_SUFFIX;
    public static final String BILLS_MDA_PROFILE_TABLE = BILLS_TABLE_PREFIX +"_mda_profile_"+TABLE_SUFFIX;
    public static final String BILLS_MDA_REVENUE_HEAD_TABLE = BILLS_TABLE_PREFIX +"_revenue_head_"+TABLE_SUFFIX;
    public static final String BILLS_REMITTA_PROFILE_TABLE = BILLS_TABLE_PREFIX +"_remitta_profile_"+TABLE_SUFFIX;
    // seed
    public static final String SEED_TABLE_PREFIX = TABLE_PREFIX + "_seed";
    public static final String SEED_LGA_TABLE =  SEED_TABLE_PREFIX+ "_lga_" + TABLE_SUFFIX;
    public static final String SEED_STATE_TABLE =  SEED_TABLE_PREFIX+ "_state_" + TABLE_SUFFIX;

    // tax profile
    public static final String TAX_PROFILE_TABLE_PREFIX = TABLE_PREFIX + "_tax_profile";
    public static final String TAX_PROFILE_TABLE = TAX_PROFILE_TABLE_PREFIX + "_" +TABLE_SUFFIX;
    public static final String TAX_PROFILE_CATEGORY_TABLE = TAX_PROFILE_TABLE_PREFIX + "_category_" +TABLE_SUFFIX;

    public static final String CUSTOMER_TABLE_PREFIX = TABLE_PREFIX + "_customer";
    public static final String CUSTOMER_TABLE = CUSTOMER_TABLE_PREFIX+"_"+TABLE_SUFFIX;
}
