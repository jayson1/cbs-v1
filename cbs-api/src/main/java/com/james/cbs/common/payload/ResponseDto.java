package com.james.cbs.common.payload;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.http.HttpStatus;


@Data
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder(value = {"message", "code", "status", "data"})
@AllArgsConstructor @NoArgsConstructor
public class ResponseDto<T> {
	private String message;
	private int code;
	private HttpStatus status;
	private T data;
}
