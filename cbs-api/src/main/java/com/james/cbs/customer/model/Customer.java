package com.james.cbs.customer.model;

import com.james.cbs.authentication.model.User;
import com.james.cbs.taxprofile.model.TaxProfile;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;

import static com.james.cbs.common.constant.TableConstant.CUSTOMER_TABLE;

@Data
@Entity
@Table( name = CUSTOMER_TABLE)
@NoArgsConstructor
@ToString
public class Customer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "uuid", unique = true)
    private String uuid;

    @OneToOne
    @JoinColumn(name = "tax_profile_id")
    private TaxProfile taxProfile;

    @OneToOne
    @JoinColumn(name = "user_id")
    private User user;

}
