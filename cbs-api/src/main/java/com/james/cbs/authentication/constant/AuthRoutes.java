package com.james.cbs.authentication.constant;

import static com.james.cbs.common.constant.RouteConstant.API_PREFIX;

public class AuthRoutes {
    public static final String AUTH_BASE_ROUTE = API_PREFIX + "/auth";
    public static final String AUTH_LOGIN = "/login";
    public static final String AUTH_SEED_USER = "/seed/user";
    public static final String AUTH_SEED_ROLE = "/seed/role";
    public static final String AUTH_SEED_PERMISSION = "/seed/permission";
    public static final String AUTH_SEEDER = "/seeder";
    public static final String TEST_BASE_ROUTE = API_PREFIX + "/test";
}
