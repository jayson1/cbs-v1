package com.james.cbs.authentication.iservice;


import com.james.cbs.authentication.dto.LoginDto;
import com.james.cbs.common.payload.ResponseDto;

public interface IAuthService {
    ResponseDto userLogin(LoginDto loginDto);
}
