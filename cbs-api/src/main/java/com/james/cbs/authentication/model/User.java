package com.james.cbs.authentication.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import javax.persistence.*;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.Collection;

import static com.james.cbs.common.constant.TableConstant.USER_ROLE_TABLE;
import static com.james.cbs.common.constant.TableConstant.USER_TABLE;

@Entity()
@Table(name = USER_TABLE)
@Data
@NoArgsConstructor
@AllArgsConstructor
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, unique = true)
    private String userName;

    @Column
    private String password;

    @Column
    private Boolean enabled = true;

    @Column
    private Boolean tokenNotExpired = true;

    @Column
    private Boolean accountNotExpired = true;

    @Column
    private Boolean accountNonLocked = true;

    @Column
    private String firstName;

    @Column
    private String lastName;

    @Column
    private String phoneNumber;

    @Column
    private LocalDate expiryDate;

    @Column
    private String accountType;

    @JsonManagedReference
    @ManyToMany(cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
    @JoinTable(name = USER_ROLE_TABLE,
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "role_id"))
    private Collection<Role> roles;
}