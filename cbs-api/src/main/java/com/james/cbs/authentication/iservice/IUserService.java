package com.james.cbs.authentication.iservice;

import com.james.cbs.authentication.model.Permission;
import com.james.cbs.authentication.model.Role;
import com.james.cbs.authentication.model.User;

import java.util.Collection;
import java.util.List;

public interface IUserService {

	User findById(Long id);

	User findByUsername(String username);

	Collection<Permission> findUserPermissions(User user);

	Collection<Permission> findUserPermissionsFromRoles(Collection<Role> roles);

	List<String> findUserPermissionsNameOnly(User user);
}
