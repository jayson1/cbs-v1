package com.james.cbs.authentication.service;

import com.james.cbs.authentication.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class UserPrincipal implements UserDetails {

	@Autowired
	private UserServiceImpl userService;

	private User user;

	public UserPrincipal( User user ) {
		this.user = user;
	}

	@Override
	public Collection< ? extends GrantedAuthority > getAuthorities( ) {
		List< String > permissions = this.userService.findUserPermissionsNameOnly( this.user );
		if ( permissions.isEmpty() ) {
			return new ArrayList<>();
		}
		return permissions.stream().map( SimpleGrantedAuthority::new ).collect( Collectors.toList() );
	}

	@Override
	public String getPassword( ) {
		return this.user.getPassword();
	}

	@Override
	public String getUsername( ) {
		return this.user.getUserName();
	}

	@Override
	public boolean isAccountNonExpired( ) {
		return this.user.getAccountNotExpired();
	}

	@Override
	public boolean isAccountNonLocked( ) {
		return this.user.getAccountNonLocked();
	}

	@Override
	public boolean isCredentialsNonExpired( ) {
		return this.user.getTokenNotExpired();
	}

	@Override
	public boolean isEnabled( ) {
		return this.user.getEnabled();
	}
}
