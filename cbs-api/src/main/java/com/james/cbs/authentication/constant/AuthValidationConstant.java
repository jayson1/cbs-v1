package com.james.cbs.authentication.constant;

public class AuthValidationConstant {
    public static final String NOT_NULL_USERNAME = "username cannot be null";
    public static final String NOT_NULL_PASSWORD = "password cannot be null";
}
