package com.james.cbs.authentication.controller;

import com.james.cbs.authentication.service.AuthServiceImpl;
import com.james.cbs.authentication.dto.LoginDto;
import com.james.cbs.common.payload.ResponseDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import static com.james.cbs.authentication.constant.AuthRoutes.*;

@RestController
@RequestMapping(value = AUTH_BASE_ROUTE)
public class AuthController {
    @Autowired
    private AuthServiceImpl authService;

    @PostMapping(value = AUTH_LOGIN)
    public ResponseDto login(@RequestBody @Validated LoginDto loginDto){
        return this.authService.userLogin(loginDto);
    }

    @GetMapping(value = AUTH_SEEDER)
    public ResponseEntity<String> runDummySeeder(){
        try{
            this.authService.runDummySeeder();
            return ResponseEntity.ok("Successful");
        }catch (Exception e){
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage());
        }
    }


}
