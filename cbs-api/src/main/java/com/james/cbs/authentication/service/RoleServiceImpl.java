package com.james.cbs.authentication.service;

import com.james.cbs.authentication.iservice.IRoleService;
import com.james.cbs.authentication.model.Permission;
import com.james.cbs.authentication.model.Role;
import com.james.cbs.authentication.repository.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class RoleServiceImpl implements IRoleService {
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private PermissionServiceImpl permissionService;

    public void createOne(){}

    public Role createDummyRole(){
        Role role = new Role();
        role.setDescription("This is a dummy role to test user authentication");
        role.setName("DummyRole");
        role.setPermissions(this.getDummyPermission());
        return this.roleRepository.save(role);
    }

    public Collection<Permission> getDummyPermission(){
        return this.permissionService.findAllPermission();
    }
}
