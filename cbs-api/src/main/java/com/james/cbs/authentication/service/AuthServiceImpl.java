package com.james.cbs.authentication.service;

import com.james.cbs.authentication.iservice.IAuthService;
import com.james.cbs.authentication.model.User;
import com.james.cbs.authentication.dto.LoginDto;
import com.james.cbs.common.payload.ResponseDto;
import com.james.cbs.security.JwtProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.HashMap;
import java.util.Map;

import static com.james.cbs.common.constant.SecurityConstant.TOKEN_PREFIX;

@Service
public class AuthServiceImpl implements IAuthService {
	@Autowired
	private UserServiceImpl userService;
	@Autowired
	private PasswordEncoder passwordEncoder;
	@Autowired
	private AuthenticationManager authenticationManager;
	@Autowired
	private JwtProvider jwtProvider;


	@Override
	public ResponseDto<Map<String, Object>> userLogin(LoginDto loginDto) {
		try{
			/*
				validate user account base on your requirement
				check is user exist
				check is account enabled
				check is account expired
			 */
			Authentication authenticate = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(loginDto.getUsername(), loginDto.getPassword()));
			SecurityContextHolder.getContext().setAuthentication(authenticate);
			Map< String, Object > userMap = new HashMap<>();
			User user = this.userService.findByUsername(loginDto.getUsername());
			String jwtToken = jwtProvider.generateToken(authenticate);
			userMap.put("username", user.getUserName());
			userMap.put("type", user.getAccountType());
			userMap.put("token", TOKEN_PREFIX+jwtToken);
			return new ResponseDto<>("Login Successful", HttpStatus.OK.value(), HttpStatus.OK, userMap);
		}catch (Exception e){
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
		}
	}

	public void runDummySeeder(){
		this.userService.createDummyUser();
	}
}
