package com.james.cbs.authentication.service;

import com.james.cbs.authentication.iservice.IPermissionService;
import com.james.cbs.authentication.model.Permission;
import com.james.cbs.authentication.repository.PermissionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;

@Service
public class PermissionServiceImpl implements IPermissionService {
    @Autowired
    private PermissionRepository permissionRepository;

    public void createOne(){}

    public Permission createDummyPermission(){
        Permission permission = new Permission();
        permission.setDescription("this is a dummy permission to test user's authentication");
        permission.setName("DummyPermission");
        permission.setModule("DummyModule");
        return this.permissionRepository.save(permission);
    }

    public List<Permission> findAllPermission(){
        return this.permissionRepository.findAll();
    }
}
