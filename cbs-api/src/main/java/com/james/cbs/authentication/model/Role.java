package com.james.cbs.authentication.model;

import java.util.Collection;
import javax.persistence.*;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import static com.james.cbs.common.constant.TableConstant.ROLE_PERMISSION_TABLE;
import static com.james.cbs.common.constant.TableConstant.ROLE_TABLE;

@Entity()
@Table(name = ROLE_TABLE)
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Role {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(unique = true, nullable = false)
    private String name;

    @Column
    private String description;

    @ManyToMany(cascade = CascadeType.MERGE, fetch=FetchType.EAGER)
    @JoinTable(name = ROLE_PERMISSION_TABLE,
            joinColumns = @JoinColumn(name = "role_id"),
            inverseJoinColumns = @JoinColumn(name = "permission_id"))
    private Collection<Permission> permissions;

}