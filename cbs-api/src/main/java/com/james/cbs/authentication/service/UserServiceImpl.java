package com.james.cbs.authentication.service;

import com.james.cbs.authentication.iservice.IUserService;
import com.james.cbs.authentication.model.Permission;
import com.james.cbs.authentication.model.Role;
import com.james.cbs.authentication.model.User;
import com.james.cbs.authentication.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.james.cbs.common.constant.ExceptionConstant.USER_404;


@Service
public class UserServiceImpl implements IUserService {
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private RoleServiceImpl roleService;
	@Autowired
	private PermissionServiceImpl permissionService;
	@Autowired
	private PasswordEncoder passwordEncoder;

	public String encodePassword(String password) {
		return passwordEncoder.encode(password);
	}

	@Override
	public User findById(Long id) {
		Optional<User> user = this.userRepository.findById(id);
		if (user.isEmpty()){
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, USER_404);
		}
		return user.get();
	}

	public User createOne(){
		return null;
	}

	@Override
	public User findByUsername(String username) {
		Optional<User> user = this.userRepository.findByUserName(username);
		if (user.isEmpty()){
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, USER_404);
		}
		return user.get();
	}

	@Override
	public Collection<Permission> findUserPermissions(User user) {
		return this.findUserPermissionsFromRoles(user.getRoles());
	}

	@Override
	public Collection<Permission> findUserPermissionsFromRoles(Collection<Role> roles) {
		Collection<Permission> permissions = new ArrayList<>();
		if(roles.isEmpty()){
			return permissions;
		}
		roles.stream().map(Role::getPermissions).forEachOrdered(permissions::addAll);
		return permissions;
	}

	@Override
	public List<String> findUserPermissionsNameOnly(User user) {
		Collection<Permission> permissions = this.findUserPermissionsFromRoles(user.getRoles());
		if (permissions.isEmpty()){
			return new ArrayList<>();
		}
		return permissions.stream().map(Permission::getName).collect(Collectors.toList());
	}

	public void createDummyUser(){
		System.out.println("------------------- creating dummy permission -------------------");
		this.permissionService.createDummyPermission();
		System.out.println("------------------- created dummy permission (done!) -------------------");

		System.out.println("------------------- creating dummy role -------------------");
		Role role = this.roleService.createDummyRole();
		System.out.println("------------------- created dummy role (done) -------------------");

		System.out.println("------------------- creating dummy user -------------------");
		User user = new User();
		user.setUserName("dummy1");
		user.setFirstName("FDummy");
		user.setLastName("LDoe");
		user.setExpiryDate(LocalDate.now().plusMonths(4));
		user.setPhoneNumber("2090829023892");
		user.setPassword(this.encodePassword("12345"));
		user.setRoles(List.of(role));
		user.setAccountType("DummyAccount Type");
		this.userRepository.save(user);
		System.out.println("------------------- created dummy user (done) -------------------");
	}
}
