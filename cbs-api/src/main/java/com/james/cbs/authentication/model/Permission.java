package com.james.cbs.authentication.model;

import javax.persistence.*;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import static com.james.cbs.common.constant.TableConstant.PERMISSION_TABLE;

@Entity()
@Table(name = PERMISSION_TABLE)
@Data @NoArgsConstructor @AllArgsConstructor @ToString
public class Permission {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private Long id;

    @Column(nullable = false, unique = true)
    private String name;

    @Column
    private String module;

    @Column
    private String description;
}
