package com.james.cbs.authentication.dto;

import lombok.Data;
import lombok.ToString;

import javax.validation.constraints.NotNull;

import static com.james.cbs.authentication.constant.AuthValidationConstant.NOT_NULL_PASSWORD;
import static com.james.cbs.authentication.constant.AuthValidationConstant.NOT_NULL_USERNAME;

@Data @ToString
public class LoginDto {
	@NotNull(message = NOT_NULL_USERNAME)
	private String username;

	@NotNull(message = NOT_NULL_PASSWORD)
	private String password;

}
