package com.james.cbs.authentication.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static com.james.cbs.authentication.constant.AuthRoutes.TEST_BASE_ROUTE;

@RestController
@RequestMapping(value = TEST_BASE_ROUTE)
public class TestController {

    @GetMapping()
    public ResponseEntity<String> testRoute(){
        return ResponseEntity.ok("It works");
    }

}
