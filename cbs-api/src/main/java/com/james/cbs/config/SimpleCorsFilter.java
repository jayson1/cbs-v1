package com.james.cbs.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;

@Component
@Order(Ordered.HIGHEST_PRECEDENCE)
public class SimpleCorsFilter implements Filter {

    private final String API_PATTERN = "^\\/api\\/(.+)$";
    private final String POINT_EXCLUSION_PATTERN = "^([^.]+)$";

    @Value( "${cbs.frontend.url}" )
    private String webUrlEndpoint;

    public SimpleCorsFilter() {
    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
        HttpServletResponse response = (HttpServletResponse) res;
        HttpServletRequest request = (HttpServletRequest) req;
        response.setHeader("Access-Control-Allow-Origin", webUrlEndpoint);
        response.setHeader("Access-Control-Allow-Credentials", "true");
        response.setHeader("Vary", "Origin");
        response.setHeader("Access-Control-Allow-Methods", "POST, PATCH, PUT, GET, OPTIONS, DELETE");
        response.setHeader("Access-Control-Max-Age", "3600");
        response.setHeader("Access-Control-Allow-Headers", "x-requested-with, authorization, content-type");

        if ("OPTIONS".equalsIgnoreCase(request.getMethod())) {
            response.setStatus(HttpServletResponse.SC_OK);
        } else {
            chain.doFilter(req, res);
        }
    }

//    @Override
//    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
//        HttpServletRequest servletRequest = (HttpServletRequest) request;
//        String requestURI = servletRequest.getRequestURI();
//        String contextPath = servletRequest.getContextPath();
//        if(!requestURI.equals(contextPath) &&
//                !requestURI.matches(API_PATTERN) && // Check if the requested URL is not a controller (/api/**)
//                requestURI.matches(POINT_EXCLUSION_PATTERN) // Check if there are no "." in requested URL
//        ) {
//            RequestDispatcher dispatcher = request.getRequestDispatcher("/");
//            dispatcher.forward(request, response);
//            return;
//        }
//        chain.doFilter(request, response);
//    }
//
    @Override
    public void init(FilterConfig filterConfig) {
        // System.out.println("simpleCorsFilter - initializing");
    }

    @Override
    public void destroy() {
        // System.out.println("simpleCorsFilter  - destroying..");
    }

}
