package com.james.cbs.config;

import com.james.cbs.authentication.service.UserDetailServiceImpl;
import com.james.cbs.security.JwtAuthenticationEntryPoint;
import com.james.cbs.security.JwtAuthenticationFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.servlet.PathRequest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.BeanIds;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import static com.james.cbs.authentication.constant.AuthRoutes.AUTH_BASE_ROUTE;
import static com.james.cbs.common.constant.RouteConstant.API_PREFIX;
import static com.james.cbs.common.constant.SocketConstant.SOCKET_ENDPOINT;

@Configuration
@EnableScheduling
@EnableWebSecurity
@EnableJpaAuditing(auditorAwareRef="auditorAware")
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true, jsr250Enabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private static final String[] ASSETS = {
            "/",
            "/**.css",
            "/**.js",
            "/**.png",
            "/**.jpg",
            "/**.jpeg",
            "/**.svg",
            "/**.woff",
            "/**.ttf",
            "/static/**",
    };

    @Bean
    public JwtAuthenticationFilter jwtAuthenticationFilter() {
        return new JwtAuthenticationFilter(getUserDetails());
    }

    @Bean
    public UserDetailsService getUserDetails() {
        // Implementation class
        return new UserDetailServiceImpl();
    }

    @Bean(BeanIds.AUTHENTICATION_MANAGER)
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public AuditorAware<String> auditorAware() {
        return new AuditorAwareImpl();
    }


    @Override
    public void configure(HttpSecurity http) throws Exception {
        // Enable CORS and disable CSRF
        http.cors().and().csrf().disable();
        http.headers().frameOptions().sameOrigin().and();

        // Set session management to stateless
        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and();

        // Set unauthorized requests exception handler
        http.exceptionHandling().authenticationEntryPoint(new JwtAuthenticationEntryPoint()).and();

        // Set permissions on endpoints
        http.authorizeRequests()
                // Our public endpoints
                .requestMatchers(PathRequest.toStaticResources().atCommonLocations()).permitAll()
                .antMatchers(ASSETS).permitAll()
                .antMatchers(SOCKET_ENDPOINT + "/**").permitAll()
                .antMatchers(AUTH_BASE_ROUTE + "/**").permitAll()
                .antMatchers(API_PREFIX + "/**").fullyAuthenticated()
                .anyRequest().authenticated();

        // Add JWT token filter
        http.addFilterBefore(jwtAuthenticationFilter(), UsernamePasswordAuthenticationFilter.class);
    }

    @Override
    public void configure(WebSecurity web) {
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder authenticationManagerBuilder) throws Exception {
        authenticationManagerBuilder.userDetailsService(getUserDetails()).passwordEncoder(passwordEncoder());
    }

}
