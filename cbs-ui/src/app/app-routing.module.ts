import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {WebsiteLayoutComponent} from '@cbs/core/website/website-layouts/website-layout/website-layout.component';
import {AdminLayoutComponent} from '@cbs/core/admin/admin-layout/admin-layout/admin-layout.component';
import {WebsiteForgotPasswordComponent} from '@cbs/core/website/website-pages/website-forgot-password/website-forgot-password.component';
import {WebsiteLoginPageComponent} from '@cbs/core/website/website-pages/website-login-page/website-login-page.component';
import {WebsiteRegisterPageComponent} from '@cbs/core/website/website-pages/website-register-page/website-register-page.component';

const routes: Routes = [
    {
        path: '',
        component: WebsiteLayoutComponent,
        loadChildren: () => import('src/app/core/website/website.module').then(m=>m.WebsiteModule)
    },
    {
        path: 'admin',
        component: AdminLayoutComponent,
        loadChildren: () => import('src/app/core/admin/admin.module').then(m=>m.AdminModule)
    },
    {
        path: 'forgot-password',
        component: WebsiteForgotPasswordComponent
    },
    {
        path: 'sign-in',
        component: WebsiteLoginPageComponent
    },
    {
        path: 'sign-up',
        component: WebsiteRegisterPageComponent
    }
];

@NgModule({
    declarations: [],
    imports: [RouterModule.forRoot(routes, {
        enableTracing: false,
        scrollPositionRestoration: 'enabled',
        useHash: true,
    })],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
