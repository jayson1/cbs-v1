export interface WebsiteInterface {}

export interface IBreadCrumbs{
    title: string;
    crumbs: ICrumb[];
}
export  interface ICrumb{
    title: string;
    link: string;
}
