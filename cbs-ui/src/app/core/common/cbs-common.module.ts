import {NgModule} from '@angular/core';
import { BillsSearchScrollComponent } from './components/bills-search-scroll/bills-search-scroll.component';
import {NgSelectModule} from '@ng-select/ng-select';
import {NgOptionHighlightModule} from '@ng-select/ng-option-highlight';
import { BillsTaxEntityCategoryComponent } from './components/bills-tax-entity-category/bills-tax-entity-category.component';
import { WebsiteHomeIconComponent } from './components/website-home-icon/website-home-icon.component';
import {RouterModule} from '@angular/router';

@NgModule({
    imports: [
        RouterModule,
        NgSelectModule,
        NgOptionHighlightModule
    ],
    providers: [],
    exports: [
        BillsSearchScrollComponent,
        BillsTaxEntityCategoryComponent,
        WebsiteHomeIconComponent
    ],
    declarations: [
      BillsSearchScrollComponent,
      BillsTaxEntityCategoryComponent,
      WebsiteHomeIconComponent
    ]
})
export class CbsCommonModule {
}

