import {NgModule} from '@angular/core';
import {WebsiteModule} from '@cbs/core/website/website.module';
import {AdminModule} from '@cbs/core/admin/admin.module';
import {CbsCommonModule} from '@cbs/core/common/cbs-common.module';
import {RouterModule} from '@angular/router';


@NgModule({
    declarations: [],
    imports: [RouterModule, WebsiteModule, AdminModule, CbsCommonModule],
    providers: [],
    exports: [CbsCommonModule, WebsiteModule, AdminModule],
})
export class CbsCoreModule {
}
