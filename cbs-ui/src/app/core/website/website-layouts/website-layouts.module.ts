import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {WebsiteHeaderComponent} from './website-header/website-header.component';
import {WebsiteFooterComponent} from './website-footer/website-footer.component';
import {WebsiteContentComponent} from './website-content/website-content.component';
import {CbsCommonModule} from '@cbs/core/common/cbs-common.module';
import {RouterModule} from '@angular/router';
import {WebsiteLayoutComponent} from '@cbs/core/website/website-layouts/website-layout/website-layout.component';


@NgModule({
    declarations: [
        WebsiteHeaderComponent,
        WebsiteFooterComponent,
        WebsiteContentComponent,
        WebsiteLayoutComponent
    ],
    imports: [
        CommonModule, CbsCommonModule, RouterModule
    ],
    exports: [WebsiteLayoutComponent]
})
export class WebsiteLayoutsModule {
}
