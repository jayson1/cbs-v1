import {Component, HostListener, OnInit, ViewChild} from '@angular/core';

@Component({
    selector: 'app-website-header',
    templateUrl: './website-header.component.html',
    styleUrls: ['./website-header.component.css']
})
export class WebsiteHeaderComponent implements OnInit {

    @ViewChild('anchor')
    anchor: Element | any;

    isOpen = false;

    constructor() {
    }

    ngOnInit(): void {
    }

    handler() {
        let selector = window.document.querySelector('.navbar-toggle');
        if (selector) {
            selector.classList.toggle('open');
            this.isOpen = !this.isOpen;
        }

    }

}
