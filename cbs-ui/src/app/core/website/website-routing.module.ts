import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {WebsiteHomePageComponent} from '@cbs/core/website/website-pages/website-home-page/website-home-page.component';
import {WebsiteAboutPageComponent} from '@cbs/core/website/website-pages/website-about-page/website-about-page.component';
import {WebsiteSearchInvoicePageComponent} from '@cbs/core/website/website-pages/website-search-invoice-page/website-search-invoice-page.component';
import {WebsiteSearchReceiptPageComponent} from '@cbs/core/website/website-pages/website-search-receipt-page/website-search-receipt-page.component';
import {WebsiteMakePaymentPageComponent} from '@cbs/core/website/website-pages/website-make-payment-page/website-make-payment-page.component';
import {WebsiteGenerateInvoicePageComponent} from '@cbs/core/website/website-pages/website-generate-invoice-page/website-generate-invoice-page.component';
import {WebsiteContactPageComponent} from '@cbs/core/website/website-pages/website-contact-page/website-contact-page.component';
import {WebsiteConfirmInvoicePageComponent} from '@cbs/core/website/website-pages/website-confirm-invoice-page/website-confirm-invoice-page.component';

const routes: Routes = [
    {
        path: '',
        component: WebsiteHomePageComponent,
    },
    {
        path: 'about',
        component: WebsiteAboutPageComponent
    },
    {
        path: 'contact',
        component: WebsiteContactPageComponent
    },
    {
        path: 'make-payment',
        component: WebsiteMakePaymentPageComponent
    },
    {
        path: 'generate-invoice',
        component: WebsiteGenerateInvoicePageComponent
    },
    {
        path: 'confirm-details',
        component: WebsiteConfirmInvoicePageComponent
    },
    {
        path: 'search-invoice',
        component: WebsiteSearchInvoicePageComponent
    },
    {
        path: 'search-receipt',
        component: WebsiteSearchReceiptPageComponent
    },

]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class WebsiteRoutingModule {}
