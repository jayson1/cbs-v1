import {Component, OnInit} from '@angular/core';
import {IBreadCrumbs} from '@cbs/core/common/payload/interface/website.interface';

@Component({
    selector: 'app-website-login-page',
    templateUrl: './website-login-page.component.html',
    styleUrls: ['./website-login-page.component.css']
})
export class WebsiteLoginPageComponent implements OnInit {

    public breadCrumbs: IBreadCrumbs = {
        title: 'Login',
        crumbs: [
            {title: 'Home', link: ''},
            {title: 'Login', link: ''}
        ]
    };


    constructor() {
    }

    ngOnInit(): void {
    }

}
