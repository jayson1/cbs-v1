import { Component, OnInit } from '@angular/core';
import {IBreadCrumbs} from '@cbs/core/common/payload/interface/website.interface';

@Component({
  selector: 'app-website-confirm-invoice-page',
  templateUrl: './website-confirm-invoice-page.component.html',
  styleUrls: ['./website-confirm-invoice-page.component.css']
})
export class WebsiteConfirmInvoicePageComponent implements OnInit {

    public breadCrumbs: IBreadCrumbs = {
        title: 'Confirm Details',
        crumbs: [
            {title: 'Home', link: ''},
            {title: 'Payment', link: ''},
            {title: 'Confirm Details', link: ''},
        ]
    }

  constructor() { }

  ngOnInit(): void {
  }

}
