import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WebsiteHomePageComponent } from './website-home-page/website-home-page.component';
import { WebsiteAboutPageComponent } from './website-about-page/website-about-page.component';
import {CbsCommonModule} from '@cbs/core/common/cbs-common.module';
import { WebsiteLoginPageComponent } from './website-login-page/website-login-page.component';
import { WebsiteRegisterPageComponent } from './website-register-page/website-register-page.component';
import { WebsiteSearchInvoicePageComponent } from './website-search-invoice-page/website-search-invoice-page.component';
import { WebsiteSearchReceiptPageComponent } from './website-search-receipt-page/website-search-receipt-page.component';
import { WebsiteMakePaymentPageComponent } from './website-make-payment-page/website-make-payment-page.component';
import { WebsiteGenerateInvoicePageComponent } from './website-generate-invoice-page/website-generate-invoice-page.component';
import { WebsiteContactPageComponent } from './website-contact-page/website-contact-page.component';
import {NgSelectModule} from '@ng-select/ng-select';
import {WebsiteComponentsModule} from '@cbs/core/website/website-components/website-components.module';
import { WebsiteConfirmInvoicePageComponent } from './website-confirm-invoice-page/website-confirm-invoice-page.component';
import {RouterModule} from '@angular/router';
import { WebsiteForgotPasswordComponent } from './website-forgot-password/website-forgot-password.component';



@NgModule({
  declarations: [
    WebsiteHomePageComponent,
    WebsiteAboutPageComponent,
    WebsiteLoginPageComponent,
    WebsiteRegisterPageComponent,
    WebsiteSearchInvoicePageComponent,
    WebsiteSearchReceiptPageComponent,
    WebsiteMakePaymentPageComponent,
    WebsiteGenerateInvoicePageComponent,
    WebsiteContactPageComponent,
    WebsiteConfirmInvoicePageComponent,
    WebsiteForgotPasswordComponent
  ],
    imports: [
        CommonModule,
        RouterModule,
        CbsCommonModule,
        NgSelectModule,
        WebsiteComponentsModule,
    ]
})
export class WebsitePagesModule { }
