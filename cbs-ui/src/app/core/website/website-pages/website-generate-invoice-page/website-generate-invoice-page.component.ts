import { Component, OnInit } from '@angular/core';
import {IBreadCrumbs} from '@cbs/core/common/payload/interface/website.interface';

@Component({
  selector: 'app-website-generate-invoice-page',
  templateUrl: './website-generate-invoice-page.component.html',
  styleUrls: ['./website-generate-invoice-page.component.css']
})
export class WebsiteGenerateInvoicePageComponent implements OnInit {

    public breadCrumbs: IBreadCrumbs = {
        title: 'Generate Invoice',
        crumbs: [
            {title: 'Home', link: ''},
            {title: 'Payment', link: ''},
            {title: 'Generate Invoice', link: ''}
        ]
    }

  constructor() { }

  ngOnInit(): void {
  }

}
