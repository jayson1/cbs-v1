import { Component, OnInit } from '@angular/core';
import {IBreadCrumbs} from '@cbs/core/common/payload/interface/website.interface';

@Component({
  selector: 'app-website-search-invoice-page',
  templateUrl: './website-search-invoice-page.component.html',
  styleUrls: ['./website-search-invoice-page.component.css']
})
export class WebsiteSearchInvoicePageComponent implements OnInit {
    public breadCrumbs: IBreadCrumbs = {
        title: 'Search Invoice',
        crumbs: [
            {title: 'Home', link: ''},
            {title: 'Payment', link: ''},
            {title: 'Search Invoice', link: ''},
        ]
    }

  constructor() { }

  ngOnInit(): void {
  }

}
