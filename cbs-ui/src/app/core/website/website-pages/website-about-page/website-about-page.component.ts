import { Component, OnInit } from '@angular/core';
import {IBreadCrumbs} from '@cbs/core/common/payload/interface/website.interface';

@Component({
  selector: 'app-website-about-page',
  templateUrl: './website-about-page.component.html',
  styleUrls: ['./website-about-page.component.css']
})
export class WebsiteAboutPageComponent implements OnInit {

    public breadCrumbs: IBreadCrumbs = {
        title: 'About',
        crumbs: [
            {title: 'Home', link: ''},
            {title: 'About', link: ''}
        ]
    }

  constructor() { }

  ngOnInit(): void {
  }

}
