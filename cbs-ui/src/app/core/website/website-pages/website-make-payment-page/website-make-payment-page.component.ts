import {Component, OnInit} from '@angular/core';
import {IBreadCrumbs} from '@cbs/core/common/payload/interface/website.interface';

declare var RmPaymentEngine: any;

@Component({
    selector: 'app-website-make-payment-page',
    templateUrl: './website-make-payment-page.component.html',
    styleUrls: ['./website-make-payment-page.component.css']
})
export class WebsiteMakePaymentPageComponent implements OnInit {
    public breadCrumbs: IBreadCrumbs = {
        title: 'Make Payment',
        crumbs: [
            {title: 'Home', link: ''},
            {title: 'Payment', link: ''},
            {title: 'Make Payment', link: ''}
        ]
    };

    public refNumber = 3400082719980;

    constructor() {
    }

    ngOnInit(): void {
    }

    public makePayment() {
        let paymentEngine: any = RmPaymentEngine.init({
            key: 'U09MRHw0MDgxOTUzOHw2ZDU4NGRhMmJhNzVlOTRiYmYyZjBlMmM1YzUyNzYwZTM0YzRjNGI4ZTgyNzJjY2NjYTBkMDM0ZDUyYjZhZWI2ODJlZTZjMjU0MDNiODBlMzI4YWNmZGY2OWQ2YjhiYzM2N2RhMmI1YWEwYTlmMTFiYWI2OWQxNTc5N2YyZDk4NA==',
            processRrr: true,
            transactionId: Math.floor(Math.random() * 1101233), // Replace with a reference you generated or remove the entire field for us to auto-generate a reference for you. Note that you will be able to check the status of this transaction using this transaction Id
            extendedData: {
                customFields: [
                    {
                        name: 'rrr',
                        value: this.refNumber
                    }
                ]
            },
            onSuccess: function(response: any) {
                console.log('callback Successful Response', response);
            },
            onError: function(response: any) {
                console.log('callback Error Response', response);
            },
            onClose: function() {
                console.log('closed');
            }
        });
        paymentEngine.showPaymentWidget();
    }

}

