import {Component, OnInit} from '@angular/core';
import {IBreadCrumbs} from '@cbs/core/common/payload/interface/website.interface';

@Component({
    selector: 'app-website-contact-page',
    templateUrl: './website-contact-page.component.html',
    styleUrls: ['./website-contact-page.component.css']
})
export class WebsiteContactPageComponent implements OnInit {
    public breadCrumbs: IBreadCrumbs = {
        title: 'Contact',
        crumbs: [
            {title: 'Home', link: ''},
            {title: 'Contact', link: ''}
        ]
    };

    constructor() {
    }

    ngOnInit(): void {
    }

}
