import {Component, OnInit} from '@angular/core';
import {IBreadCrumbs} from '@cbs/core/common/payload/interface/website.interface';

@Component({
    selector: 'app-website-search-receipt-page',
    templateUrl: './website-search-receipt-page.component.html',
    styleUrls: ['./website-search-receipt-page.component.css']
})
export class WebsiteSearchReceiptPageComponent implements OnInit {
    public breadCrumbs: IBreadCrumbs = {
        title: 'Validate Receipt',
        crumbs: [
            {title: 'Home', link: ''},
            {title: 'Payment', link: ''},
            {title: 'Search Receipt', link: ''},
        ]
    }

    constructor() {
    }

    ngOnInit(): void {
    }

}
