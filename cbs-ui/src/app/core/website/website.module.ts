import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {WebsitePagesModule} from '@cbs/core/website/website-pages/website-pages.module';
import {WebsiteComponentsModule} from '@cbs/core/website/website-components/website-components.module';
import {WebsiteLayoutsModule} from '@cbs/core/website/website-layouts/website-layouts.module';
import {RouterModule} from '@angular/router';
import {WebsiteRoutingModule} from '@cbs/core/website/website-routing.module';


@NgModule({
    declarations: [],
    imports: [
        CommonModule,
        RouterModule,
        WebsiteRoutingModule,
        WebsitePagesModule,
        WebsiteComponentsModule,
        WebsiteLayoutsModule
    ],
    exports: [WebsiteLayoutsModule]
})
export class WebsiteModule {
}
