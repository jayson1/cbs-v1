import {Component, Input, OnInit} from '@angular/core';
import {IBreadCrumbs} from '@cbs/core/common/payload/interface/website.interface';

@Component({
    selector: 'app-website-bg-half',
    templateUrl: './website-bg-half.component.html',
    styleUrls: ['./website-bg-half.component.css']
})
export class WebsiteBgHalfComponent implements OnInit {

    @Input('props')
    public props: IBreadCrumbs = {crumbs: [], title: 'Untitled'};

    constructor() {
    }

    ngOnInit(): void {
    }

}
