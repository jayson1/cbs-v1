import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WebsiteBgHalfComponent } from './website-bg-half/website-bg-half.component';



@NgModule({
    declarations: [
        WebsiteBgHalfComponent
    ],
    exports: [
        WebsiteBgHalfComponent
    ],
    imports: [
        CommonModule
    ]
})
export class WebsiteComponentsModule { }
