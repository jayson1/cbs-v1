import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AdminPagesModule} from '@cbs/core/admin/admin-pages/admin-pages.module';
import {AdminLayoutModule} from '@cbs/core/admin/admin-layout/admin-layout.module';
import {AdminComponentsModule} from '@cbs/core/admin/admin-components/admin-components.module';
import {RouterModule} from '@angular/router';
import {AdminRoutingModule} from '@cbs/core/admin/admin-routing.module';


@NgModule({
    declarations: [],
    imports: [
        CommonModule,
        RouterModule,
        AdminRoutingModule,
        AdminPagesModule,
        AdminLayoutModule,
        AdminComponentsModule
    ],
    exports: [AdminLayoutModule]
})
export class AdminModule {
}
