import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AdminLayoutComponent} from './admin-layout/admin-layout.component';
import {CbsCommonModule} from '@cbs/core/common/cbs-common.module';
import {RouterModule} from '@angular/router';


@NgModule({
    declarations: [
        AdminLayoutComponent
    ],
    imports: [
        RouterModule,
        CommonModule,
        CbsCommonModule,
    ],
    exports: [AdminLayoutComponent]
})
export class AdminLayoutModule {
}
